import React from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  const callAPI = async () => {
    const response = await fetch("/API/index.js");
    const json = await response.json();

    console.log(json);
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <button onClick={callAPI}>
          <span role="img" aria-label="phone">
            📞
          </span>
          API
        </button>
      </header>
    </div>
  );
}

export default App;
